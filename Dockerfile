FROM nvidia/cuda:11.1.1-cudnn8-devel-ubuntu20.04

RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y curl pkg-config libssl-dev gcc && \
curl -LO 'https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-gpu-linux-x86_64-2.4.0.tar.gz' && \
tar -C /usr/local/ -xzf libtensorflow-gpu-linux-x86_64-2.4.0.tar.gz && \
ldconfig && \
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && \
. /root/.cargo/env && \
cargo install sccache && \
mkdir -p /root/.cargo/ && \
echo "[build]\nrustc-wrapper = \"/root/.cargo/bin/sccache\"" >> /root/.cargo/config && \
apt-get remove -y curl && \
apt-get clean && \
rm *.tar.gz && \
ln -s /usr/local/cuda-11.1/targets/x86_64-linux/lib/libcusolver.so.11 /usr/local/cuda-11.1/targets/x86_64-linux/lib/libcusolver.so.10 && \
echo "export LD_LIBRARY_PATH=\"\$LD_LIBRARY_PATH:/usr/local/cuda-11.1/targets/x86_64-linux/lib/\"" >> /root/.bashrc
